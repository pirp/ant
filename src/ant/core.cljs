(ns ant.core
  (:require [reagent.core :as r]))

(def initial-square-index [80 120])

(def initial-direction :w)

(defn square [x y color occupied?]
  [:div {:key   (random-uuid)
         :style {:background-color (if occupied?
                                     "red"
                                     (name color))
                 :color            "black"
                 :position         "absolute"
                 :top              (str (/ x 2) "em")
                 :left             (str (/ y 2) "em")
                 :width            "0.5em"
                 :height           "0.5em"}} ""])

(defonce step-counter (r/atom 0))
(defonce running? (r/atom false))

(defonce app-state (r/atom {:squares              {}
                            :current-direction    initial-direction
                            :current-square-index initial-square-index
                            :current-square-color :white}))

(def turn-left
  {:n :w
   :w :s
   :e :n
   :s :e})

(def turn-right
  (clojure.set/map-invert turn-left))

(defn go-forward [x y]
  {:n [x (+ y 1)]
   :s [x (- y 1)]
   :w [(- x 1) y]
   :e [(+ x 1) y]})

(def complementary {:white :black
                    :black :white})

(defn flip-color! [square-index square-color squares]
  (map (fn [s] (if (= (:index s) square-index)
                 (assoc s :occupied? false :color (square-color complementary))
                 s))
       squares))

(defn move-to-square! [square-index squares]
  (if (contains? (set (map :index squares)) square-index)
    (map (fn [s] (if (= (:index s) square-index) (assoc s :occupied? true) s)) squares)
    (conj squares {:index     square-index
                   :x         (first square-index)
                   :y         (second square-index)
                   :occupied? true
                   :color     :white})))


(defn update-position [current-direction current-square-index current-square-color squares]
  (let [new-direction (if (= current-square-color :white) (current-direction turn-right)
                                                          (current-direction turn-left))
        new-square-index (new-direction (go-forward (first current-square-index) (second current-square-index)))
        new-square-color (if (contains? (set (map :index squares)) new-square-index)
                           (:color (first (filter #(= new-square-index (:index %)) squares)))
                           :white)]
    (do
      (swap! step-counter inc)
      (swap! app-state assoc :squares (->> squares
                                           (flip-color! current-square-index current-square-color)
                                           (move-to-square! new-square-index)))
      (swap! app-state assoc :current-square-index new-square-index)
      (swap! app-state assoc :current-direction new-direction)
      (swap! app-state assoc :current-square-color new-square-color))))


(defn run-once []
  (js/setTimeout #(do (update-position (:current-direction @app-state)
                                       (:current-square-index @app-state)
                                       (:current-square-color @app-state)
                                       (:squares @app-state))
                      (if @running?
                        (run-once)
                        nil))
                 10))

(defn hello-world []
  [:div
   [:span {:display "flex"}
    (map (fn [s]
           (square (:x s) (:y s) (:color s) (:occupied? s)))
         (:squares @app-state))]
   [:h1 (:text @app-state)]

   [:div
    [:div @step-counter " steps"]
    [:button {:size     "large"
              :on-click #(do (reset! running? true)
                             (run-once))} "Start!"]
    [:button {:size     "large"
              :on-click #(reset! running? false)} "Stop!"]]])

(r/render-component [hello-world]
                    (. js/document (getElementById "app")))